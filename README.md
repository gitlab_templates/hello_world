# Hello World Pipeline Template

## Description

Hello World Pipeline Template


## Flowchart

```mermaid
flowchart LR

A[Input] --> B(Testing)
B --> C{If}
C --> D[Result 1]
C --> E[Result 2]
```